require('dotenv').config();

module.exports = {
    'strapi-plugin-populate-deep': {
        config: {
            defaultDepth: 5, // Default is 5
        }
    },
    upload: {
        config: {
            providerOptions: {
                localServer: {
                    maxage: 300000
                },
            },
            sizeLimit: 250 * 1024 * 1024,
            breakpoints: {
                xlarge: 1920,
                large: 1000,
                medium: 750,
                small: 500,
                xsmall: 64
            },
        },
    },
    email: {
        provider: 'nodemailer',
        providerOptions: {
          host: process.env.SMTP_HOST,
          port: process.env.SMTP_PORT,
          auth: {
            user: process.env.SMTP_USERNAME,
            pass: process.env.SMTP_PASSWORD,
          },
          settings: {
            from: process.env.EMAIL_FROM,
          },
        },
        // ...
      },
};