'use strict';

/**
 * post-question service
 */

const { createCoreService } = require('@strapi/strapi').factories;
const axios = require('axios');
const apiKey = process.env.OPENAI_API_KEY;


module.exports = createCoreService('api::post-question.post-question', ({ strapi }) => ({
    async updateTopics(postId) {
        let topic = 'Autre';
        const post = await strapi.entityService.findOne('api::post-question.post-question', postId, {
            populate: '*',
        });
        console.log('Post: ', post); // Vérifier que le post est récupéré correctement
        const commentaires = post && post.commentaire_id ? post.commentaire_id : [];
        console.log('Commentaires: ', commentaires); // Vérifier que les commentaires sont récupérés correctement
        commentaires.forEach((commentaire) => {
            if (commentaire.texte.includes('java') || commentaire.texte.includes('jdk')) {
                topic = 'Java';
            } else if (commentaire.texte.includes('js') || commentaire.texte.includes('javascript')) {
                topic = 'JavaScript';
            } else if (commentaire.texte.includes('php')) {
                topic = 'PHP';
            } else if (commentaire.texte.includes('c#') || commentaire.texte.includes('.net')) {
                topic = 'C#';
            } else if (commentaire.texte.includes('android')) {
                topic = 'Android';
            } else if (commentaire.texte.includes('css')) {
                topic = 'CSS';
            } else if (commentaire.texte.includes('mysql')) {
                topic = 'MySQL';
            } else if (commentaire.texte.includes('r') || commentaire.texte.includes('R')) {
                topic = 'R';
            } else if (commentaire.texte.includes('c') || commentaire.texte.includes('langageC')) {
                topic = 'C';
            } else if (commentaire.texte.includes('node.js') || commentaire.texte.includes('node')) {
                topic = 'NODEJS';
            } else if (commentaire.texte.includes('excel')) {
                topic = 'Excel';
            }
        });
        console.log('Topic: ', topic); // Vérifier que la variable topic est modifiée
        await strapi.entityService.update('api::post-question.post-question', postId, {
            data: {
                topic: topic,
            },
        });
    },

    async openAIChat(prompt) {
        const url = 'https://api.openai.com/v1/chat/completions';
        const data = {
            model: 'gpt-3.5-turbo',
            messages: [{ "role": "user", "content": prompt }]
        }

        try {
            const response = await axios.post(url, data, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${apiKey}`
                }
            });
            return response.data.choices[0].message.content;
        } catch (error) {
            return 'Une erreur s\'est produite lors de la communication avec OpenAI API.';
        }
    },

    async editInstruction(data) {
        const url = 'https://api.openai.com/v1/edits';
        const dataBody = {
            model: "text-davinci-edit-001",
            input: data.data.prompt,
            instruction: data.data.instruction,
            temperature: 0.5
        }

        try {
            const response = await axios.post(url, dataBody, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${apiKey}`
                }
            });
            console.log(response.data)
            return response.data.choices;
        } catch (error) {
            console.log(error)
            return 'Une erreur s\'est produite lors de la communication avec OpenAI API.';
        }
    }


}));

