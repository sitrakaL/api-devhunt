'use strict';

/**
 * post-question controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::post-question.post-question', ({ strapi }) => ({
    async updateTopicFromComment(ctx) {
        const { id: postId } = ctx.params;
        try {
            await strapi.service('api::post-question.post-question').updateTopics(postId);
            return ctx.send({ message: 'Topic updated successfully' });
        } catch (error) {
            return ctx.badRequest('Failed to update topic');
        }
    },

    async opeAIChatResp(ctx) {
        const data = ctx.request.body.prompt;
        try {
            return await strapi.service('api::post-question.post-question').openAIChat(data);
        } catch (error) {
            return ctx.badRequest('Failed to update topic');
        }
    },

    async editData(ctx) {
        const data = ctx.request.body;
        try {
            return await strapi.service('api::post-question.post-question').editInstruction(data);
        } catch (error) {
            return ctx.badRequest('Failed to update topic');
        }
    }
}));
