module.exports = {
    async beforeDelete(event) {
        const { where } = event.params;
        const post = await strapi.service('api::post-question.post-question').findOne(where.id);
        await strapi.query('api::commentaire.commentaire').delete({ where: { post_id: post.id }, include: ['post'] });
    },
};    