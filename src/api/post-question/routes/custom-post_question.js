module.exports = {
    routes: [
        {
            method: 'PUT',
            path: '/post/topics/:id',
            handler: 'post-question.updateTopicFromComment'
        },

        {
            method: 'POST',
            path: '/post/chat',
            handler: 'post-question.opeAIChatResp'
        },
        {
            method: 'POST',
            path: '/post/editText',
            handler: 'post-question.editData'
        }
    ]
}

