'use strict';

/**
 * post-question router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::post-question.post-question');
