'use strict';
const admin = require("firebase-admin");
const serviceAccount = require("./extensions/users-permissions/keyFirebase.json");

module.exports = {

  register(/*{ strapi }*/) { },

  bootstrap({ strapi }) {
    let firebase = admin.initializeApp({
      credential: admin.credential.cert(serviceAccount),
    });
    //Make Firebase available everywhere
    strapi.firebase = firebase;
    let messaging = firebase.messaging();

    let sendNotification = (fcm, data) => {
      let message = {
        ...data,
        token: fcm
      }
      messaging.send(message).then((res) => {
        console.log(res);
      }).catch((error) => {
        console.log(error);
      });
    }
    let sendNotificationToTopic = (topic_name, data) => {
      let message = {
        ...data,
        topic: topic_name
      }
      messaging.send(message).then((res) => {
        console.log(res);
      }).catch((error) => {
        console.log(error);
      });
    }
    let subscribeTopic = (fcm, topic_name) => {
      messaging.subscribeToTopic(fcm, topic_name).then((res) => {
        console.log(res);
      }).catch((error) => {
        console.log(error);
      });
    }
    //Make the notification functions available everywhere
    strapi.notification = {
      subscribeTopic,
      sendNotificationToTopic,
      sendNotification
    }
  },
};
